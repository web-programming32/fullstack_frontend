export default interface User {
  id?: number;
  name: string;
  phone: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
